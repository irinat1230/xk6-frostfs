import {uuidv4} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import local from 'k6/x/frostfs/local';

const payload = open('../go.sum', 'b');
const local_cli = local.connect("/path/to/config.yaml", "")

export const options = {
    stages: [
        {duration: '30s', target: 10},
    ],
};

export default function () {
    let headers = {
        'unique_header': uuidv4()
    }
    const container_id = '6BVPPXQewRJ6J5EYmAPLczXxNocS7ikyF7amS2esWQnb';
    let resp = local_cli.put(container_id, headers, payload)
    if (resp.success) {
        local_cli.get(container_id, resp.object_id)
    } else {
        console.log(resp.error)
    }
}
