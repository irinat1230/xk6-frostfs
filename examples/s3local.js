import {uuidv4} from 'https://jslib.k6.io/k6-utils/1.2.0/index.js';
import s3local from 'k6/x/frostfs/s3local';

const bucket = "testbucket"
const payload = open('../go.sum', 'b');
const s3local_cli = s3local.connect("path/to/storage/config.yml", {}, {
    'testbucket': 'GBQDDUM1hdodXmiRHV57EUkFWJzuntsG8BG15wFSwam6',
});

export default function () {
    const key = uuidv4();
    if (s3local_cli.put(bucket, key, payload).success) {
        s3local_cli.get(bucket, key)
    }
}
