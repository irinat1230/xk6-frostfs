package xk6_frostfs

import (
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/datagen"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/env"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/local"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/logging"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/native"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/registry"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/s3"
	_ "git.frostfs.info/TrueCloudLab/xk6-frostfs/internal/s3local"
	"go.k6.io/k6/js/modules"
)

const (
	version = "v0.1.0"
)

func init() {
	modules.Register("k6/x/frostfs", &FrostFS{Version: version})
}

type FrostFS struct {
	Version string
}
