import env from 'k6/x/frostfs/env';

export function parseEnv() {
    if (__ENV.ENV_FILE) {
        const parsedVars = env.parse(__ENV.ENV_FILE)
        for (const prop in parsedVars) {
            __ENV[prop] = __ENV[prop] || parsedVars[prop];
        }
    }
}
