import datagen from 'k6/x/frostfs/datagen';
import logging from 'k6/x/frostfs/logging';
import registry from 'k6/x/frostfs/registry';
import s3local from 'k6/x/frostfs/s3local';
import { SharedArray } from 'k6/data';
import { textSummary } from './libs/k6-summary-0.0.2.js';
import { parseEnv } from './libs/env-parser.js';

parseEnv();

const obj_list = new SharedArray('obj_list', function () {
    return JSON.parse(open(__ENV.PREGEN_JSON)).objects;
});

const container_list = new SharedArray('container_list', function () {
    return JSON.parse(open(__ENV.PREGEN_JSON)).containers;
});

const bucket_list = new SharedArray('bucket_list', function () {
    return JSON.parse(open(__ENV.PREGEN_JSON)).buckets;
});

function bucket_mapping() {
    if (container_list.length != bucket_list.length) {
        throw 'The number of containers and buckets in the preset file must be the same.';
    }
    let mapping = {};
    for (let i = 0; i < container_list.length; ++i) {
        mapping[bucket_list[i]] = container_list[i];
    }
    return mapping;
}

const read_size = JSON.parse(open(__ENV.PREGEN_JSON)).obj_size;
const summary_json = __ENV.SUMMARY_JSON || "/tmp/summary.json";

const config_file = __ENV.CONFIG_FILE;
const s3_client = s3local.connect(config_file, {}, bucket_mapping());
const log = logging.new().withField("config", config_file);

const registry_enabled = !!__ENV.REGISTRY_FILE;
const obj_registry = registry_enabled ? registry.open(__ENV.REGISTRY_FILE) : undefined;

const duration = __ENV.DURATION;

const generator = datagen.generator(1024 * parseInt(__ENV.WRITE_OBJ_SIZE));

const scenarios = {};

const write_vu_count = parseInt(__ENV.WRITERS || '0');
if (write_vu_count > 0) {
    scenarios.write = {
        executor: 'constant-vus',
        vus: write_vu_count,
        duration: `${duration}s`,
        exec: 'obj_write', 
        gracefulStop: '5s',
    };
}

const read_vu_count = parseInt(__ENV.READERS || '0');
if (read_vu_count > 0) {
    scenarios.read = {
        executor: 'constant-vus',
        vus: read_vu_count,
        duration: `${duration}s`,
        exec: 'obj_read', 
        gracefulStop: '5s',
    };
}

export const options = {
    scenarios,
    setupTimeout: '5s',
};

export function setup() {
    const total_vu_count = write_vu_count + read_vu_count;

    console.log(`Pregenerated buckets:          ${bucket_list.length}`);
    console.log(`Pregenerated read object size: ${read_size}`);
    console.log(`Pregenerated total objects:    ${obj_list.length}`);
    console.log(`Reading VUs:                   ${read_vu_count}`);
    console.log(`Writing VUs:                   ${write_vu_count}`);
    console.log(`Total VUs:                     ${total_vu_count}`);
}

export function teardown(data) {
    if (obj_registry) {
        obj_registry.close();
    }
}

export function handleSummary(data) {
    return {
        'stdout': textSummary(data, { indent: ' ', enableColors: false }),
        [summary_json]: JSON.stringify(data),
    };
  }

export function obj_write() {
    const key = __ENV.OBJ_NAME || uuidv4();
    const bucket = bucket_list[Math.floor(Math.random() * bucket_list.length)];

    const { payload, hash } = generator.genPayload(registry_enabled);
    const resp = s3_client.put(bucket, key, payload);
    if (!resp.success) {
        log.withFields({bucket: bucket, key: key}).error(resp.error);
        return;
    }

    if (obj_registry) {
        obj_registry.addObject("", "", bucket, key, hash);
    }
}

export function obj_read() {
    const obj = obj_list[Math.floor(Math.random() * obj_list.length)];

    const resp = s3_client.get(obj.bucket, obj.object);
    if (!resp.success) {
        log.withFields({bucket: obj.bucket, key: obj.object}).error(resp.error);
    } 
}

export function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        let r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

