#!/usr/bin/python3

import argparse
import json
import requests

parser = argparse.ArgumentParser()
parser.add_argument('--endpoint', help='Endpoint of the S3 gateway')
parser.add_argument('--preset_file', help='JSON file path with s3 preset')

args = parser.parse_args()

def main():
    with open(args.preset_file) as f:
        preset_text = f.read()

    preset = json.loads(preset_text)

    containers = []
    for bucket in preset.get('buckets'):
        resp = requests.head(f'{args.endpoint}/{bucket}', verify=False)
        containers.append(resp.headers['X-Container-Id'])

    preset['containers'] = containers
    with open(args.preset_file, 'w+') as f:
        json.dump(preset, f, ensure_ascii=False, indent=2)

if __name__ == "__main__":
    main()