package rawclient

import (
	"crypto/ecdsa"
	"time"
)

type (
	PutHandler    func(uint64, error, time.Duration)
	GetHandler    func(uint64, error, time.Duration)
	DeleteHandler func(error, time.Duration)
)

type config struct {
	key      ecdsa.PrivateKey
	onPut    PutHandler
	onGet    GetHandler
	onDelete DeleteHandler
}

type Option func(*config)

func defaultConfig() *config {
	return &config{
		onPut:    func(uint64, error, time.Duration) {},
		onGet:    func(uint64, error, time.Duration) {},
		onDelete: func(error, time.Duration) {},
	}
}

// WithKey sets the private key used by the raw client if no other key
// is available when setting owner IDs.
// Required.
func WithKey(key ecdsa.PrivateKey) Option { return func(c *config) { c.key = key } }

// WithPutHandler sets the hook invoked on completion of Put calls.
// This is useful for updating metrics or debugging.
func WithPutHandler(h PutHandler) Option { return func(c *config) { c.onPut = h } }

// WithGetHandler sets the hook invoked on completion of Get calls.
// This is useful for updating metrics or debugging.
func WithGetHandler(h GetHandler) Option { return func(c *config) { c.onGet = h } }

// WithDeleteHandler sets the hook invoked on completion of Delete calls.
// This is useful for updating metrics or debugging.
func WithDeleteHandler(h DeleteHandler) Option { return func(c *config) { c.onDelete = h } }
