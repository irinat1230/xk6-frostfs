package summary_comparison

import (
	"golang.org/x/perf/benchmath"
	"log"
)

func statCompare(values1, values2 []float64, key string) (float64, float64, float64, string) {
	if len(values1) != len(values2) {
		log.Println("Different sizes of data for", key)
		return -1, -1, -1, ""
	}
	thresholds := benchmath.DefaultThresholds
	assumption := benchmath.AssumeNothing
	sample1 := benchmath.NewSample(values1, &thresholds)
	sample2 := benchmath.NewSample(values2, &thresholds)
	summary1 := assumption.Summary(sample1, 0.95)
	summary2 := assumption.Summary(sample2, 0.95)
	comparison := assumption.Compare(sample1, sample2)
	center1 := summary1.Center
	center2 := summary2.Center
	return center1, center2, -(center1 - center2) / center1 * 100, comparison.String()
}
