package summary_comparison

import (
	"errors"
	u "github.com/docker/go-units"
	"log"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Record struct {
	SubField, Unit string
	Val            float64
}

type ParsedRecord map[string][]Record

// ParseRecord : parses given record/summary
// WARN: when encountering rec[i] = "MB" it sends a message that the value couldn't be parsed. idk how to bypass it  without костыли
func ParseRecord(text string) ParsedRecord {
	record := createMapOfValues(strings.Fields(text))
	res := make(ParsedRecord, len(record))
	for field, rec := range record {
		if strings.Contains(field, "vus") {
			continue
		}
		for i := range rec {
			if !strings.Contains(rec[i], "=") {
				continue
			}
			split := strings.Split(rec[i], "=")
			parseValue(field, split[0], split[1], res, rec, i)
		}
	}
	return res
}

// data unit parsing is somewhat tricky, 1 KB = 1000 B, not 1024
func parseValue(field, id, val string, parsed ParsedRecord, rec []string, i int) int {
	num, errNum := strconv.ParseFloat(val, 64)
	data, errData := u.FromHumanSize(val)
	time1, errTime := time.ParseDuration(val)
	err := parseSpeed(field, id, val, parsed)

	if errNum != nil && errData != nil && errTime != nil && err != nil {
		log.Println("Can't parse", field, id, ":", val)
	} else if errNum != nil && errData != nil && errTime != nil {
		return i
	} else if errNum != nil && errData != nil {
		parsed[field] = append(parsed[field], Record{id, "s", time1.Seconds()})
	} else if errNum != nil {
		parsed[field] = append(parsed[field], Record{id, "B", float64(data)})
	} else if i >= len(rec)-1 {
		parsed[field] = append(parsed[field], Record{id, "op", num})
	} else {
		if regexp.MustCompile(`\d`).MatchString(rec[i+1]) {
			return i
		}
		data, errData := u.FromHumanSize(val + rec[i+1])
		time1, errTime := time.ParseDuration(val)
		err := parseSpeed(field, id, val+rec[i+1], parsed)

		if errData != nil && errTime != nil && err != nil {
			log.Println("Can't parse", field, id, ":", val)
		} else if errData != nil && errTime != nil {
			i++
		} else if errData != nil {
			parsed[field] = append(parsed[field], Record{id, "s", time1.Seconds()})
			i++
		} else {
			parsed[field] = append(parsed[field], Record{id, "B", float64(data)})
			i++
		}
	}
	return i
}

func parseSpeed(field, id, val string, parsed ParsedRecord) error {
	var sep string
	if !assignSep(val, &sep) {
		return errors.New("invalid")
	}
	split := strings.Split(val, sep)
	num, errNum := strconv.ParseFloat(split[0], 64)
	if errNum == nil {
		return checkIfTime(id, field, split[1], "op/s", num, parsed)
	}
	data, errData := u.FromHumanSize(split[0])
	if errData != nil {
		return errData
	}
	return checkIfTime(id, field, split[1], "B/s", float64(data), parsed)
}
func checkIfTime(id, field, timeD, unit string, val float64, parsed ParsedRecord) error {
	time1, errTime := time.ParseDuration("1" + timeD)
	if errTime != nil {
		return errTime
	}
	parsed[field] = append(parsed[field], Record{id, unit, val / time1.Seconds()})
	return nil
}

func assignSep(val string, sep *string) bool {
	if strings.Contains(val, "/") {
		*sep = "/"
	} else if strings.Contains(val, "\\") {
		*sep = "\\"
	} else {
		return false
	}
	return true
}

func createMapOfValues(text []string) map[string][]string {
	record := make(map[string][]string, recordLen)
	for ind := 0; ind < len(text); ind++ {
		if !strings.Contains(text[ind], ":") {
			continue
		}
		key := strings.Trim(text[ind][:len(text[ind])-1], ".")
		ind++
		for ind < len(text) && !strings.Contains(text[ind], ":") {
			record[key] = append(record[key], text[ind])
			ind++
		}
		ind--
	}
	return record
}
