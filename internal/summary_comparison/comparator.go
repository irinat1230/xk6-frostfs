package summary_comparison

import (
	"fmt"
	"log"
	"os"
	"sort"
	"strings"
)

const recordLen = 48
const maxLen = 36

// Compare : reads files from 2 directories read from program arguments and returns their comparison
func Compare() {
	args := os.Args[1:]
	var records1, records2 []ParsedRecord
	readFiles(args[0], &records1)
	readFiles(args[1], &records2)
	var values1, values2 map[string][]float64
	values1 = getValues(records1)
	values2 = getValues(records2)
	keys := getKeys(records1[0])
	sort.Strings(keys)
	fmt.Println(strings.Join([]string{"Field" + strings.Repeat(" ", 28), "old", "new", "diff\n"}, "\t"))
	for _, key := range keys {
		val1, ok1 := values1[key]
		val2, ok2 := values2[key]
		if !ok1 || !ok2 {
			continue
		}
		f1, f2, r, cmp := statCompare(val1, val2, key)
		if cmp == "" {
			continue
		}
		var diff string
		if r < 0 {
			diff = fmt.Sprintf("%.3f", r) + "%"
		} else if r > 0 {
			diff = "+" + fmt.Sprintf("%.3f", r) + "%"
		} else {
			diff = "~"
		}
		if len(key) < maxLen {
			key += strings.Repeat(".", maxLen-len(key))
		}
		fmt.Println(strings.Join([]string{key + ":", fmt.Sprintf("%.4f", f1), fmt.Sprintf("%.4f", f2), diff, "(" + cmp + ")"}, "\t"))
	}
}

func getValues(parsedRecords []ParsedRecord) map[string][]float64 {
	values := make(map[string][]float64, len(parsedRecords)*recordLen)
	for _, fileRecords := range parsedRecords {
		for field, records := range fileRecords {
			for _, record := range records {
				key := field + " " + record.SubField + ", " + record.Unit
				values[key] = append(values[key], record.Val)
			}
		}
	}
	return values
}

func getKeys(parsedRecord ParsedRecord) []string {
	var keys []string
	for field, records := range parsedRecord {
		for _, record := range records {
			key := field + " " + record.SubField + ", " + record.Unit
			keys = append(keys, key)
		}
	}
	return keys
}

func readFiles(arg string, records *[]ParsedRecord) {
	files, err := os.ReadDir(arg)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if file.IsDir() {
			return
		}
		recordFile, errReadFile := os.ReadFile(arg + "\\" + file.Name())
		if errReadFile != nil {
			log.Fatal(errReadFile)
		}
		*records = append(*records, ParseRecord(string(recordFile)))
	}
}
