     data_received............: 482 MB 16 MB/s
     data_sent................: 483 MB 16 MB/s
     iteration_duration.......: avg=56.04ms  min=20.63ms med=60.75ms max=110.51ms p(90)=65.29ms p(95)=66.39ms
     iterations...............: 2680   89.237091/s
     local_obj_get_duration...: avg=231.79µs min=0s      med=0s      max=5.52ms   p(90)=997µs   p(95)=1ms
     local_obj_get_total......: 2680   89.237091/s
     local_obj_put_duration...: avg=55.04ms  min=20.63ms med=59.64ms max=109ms    p(90)=63.78ms p(95)=65.05ms
     local_obj_put_total......: 2680   89.237091/s
     vus......................: 9      min=1       max=9
     vus_max..................: 10     min=10      max=10
