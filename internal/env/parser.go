package env

import (
	"os"

	"github.com/joho/godotenv"
	"go.k6.io/k6/js/modules"
)

// RootModule is the global module object type. It is instantiated once per test
// run and will be used to create k6/x/frostfs/registry module instances for each VU.
type RootModule struct{}

// Parser represents an instance of the module for every VU.
type Parser struct {
	vu modules.VU
}

// Ensure the interfaces are implemented correctly.
var (
	_ modules.Instance = &Parser{}
	_ modules.Module   = &RootModule{}
)

func init() {
	modules.Register("k6/x/frostfs/env", new(RootModule))
}

// NewModuleInstance implements the modules.Module interface and returns
// a new instance for each VU.
func (r *RootModule) NewModuleInstance(vu modules.VU) modules.Instance {
	mi := &Parser{vu: vu}
	return mi
}

// Exports implements the modules.Instance interface and returns the exports
// of the JS module.
func (p *Parser) Exports() modules.Exports {
	return modules.Exports{Default: p}
}

func (p *Parser) Parse(fileName string) (map[string]string, error) {
	f, err := os.Open(fileName)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	return godotenv.Parse(f)
}
