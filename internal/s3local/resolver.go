package s3local

import (
	"context"
	"fmt"

	cid "git.frostfs.info/TrueCloudLab/frostfs-sdk-go/container/id"
)

// fixedBucketResolver is a static bucket resolver from the provided map.
// This is needed to replace the normal resolver for local storage engine clients, since
// those should not use DNS or NNS for resolution.
type fixedBucketResolver map[string]cid.ID

func newFixedBucketResolver(bucketMapping map[string]string) (fixedBucketResolver, error) {
	r := fixedBucketResolver{}
	for bucket, cidStr := range bucketMapping {
		var id cid.ID
		if err := id.DecodeString(cidStr); err != nil {
			return nil, fmt.Errorf("decoding container id %q: %v", cidStr, err)
		}
		r[bucket] = id
	}
	return r, nil
}

func (r fixedBucketResolver) Resolve(_ context.Context, bucket string) (cid.ID, error) {
	if cid, resolved := r[bucket]; resolved {
		return cid, nil
	}
	return cid.ID{}, fmt.Errorf("bucket %s is not mapped to any container", bucket)
}
